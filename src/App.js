import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './quiz-3/Routes'
import './App.css';


function App() {
  return (
    <Router>
      <Routes />
      {/* masukkan kode anda disini */}
    </Router>
    // <div>
    //   {/* <Tugas11 /> */}
    //   <Soal2 />
    //   {/* <Tugas14 /> */}
    // </div>
  );
}

export default App;
