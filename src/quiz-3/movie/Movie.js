import React, { useContext } from "react"
import {MovieProvider} from "./MovieContext"
import MovieList from "./MovieList"
import MovieForm from "./MovieForm"
import "./Movie.css"

const Movie = () =>{
  return(
    <>
      <MovieProvider>
        <MovieList/>
        <MovieForm/>
      </MovieProvider>
    </>
  )
}

export default Movie