import React, { useContext } from "react"
import {LoginProvider} from "./LoginContext"
import LoginForm from "./LoginForm"
import "./Login.css"

const Login = () =>{
  return(
    <>
      <LoginProvider>
        <LoginForm/>
      </LoginProvider>
    </>
  )
}

export default Login