import React, { useState, createContext } from "react";

export const LoginContext = createContext();

export const LoginProvider = props => {
  const [listLogin, setlistLogin] = useState({
    user: "admin",
    password: "admin"
  });

  return (
    <LoginContext.Provider value={[listLogin, setlistLogin]}>
      {props.children}
    </LoginContext.Provider>
  );
};