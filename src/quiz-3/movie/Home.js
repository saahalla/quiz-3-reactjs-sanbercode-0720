import React, { useContext } from "react"
import {MovieProvider} from "./MovieContext"
import MovieHome from "./MovieHome"
import "./Movie.css"

const Movie = () =>{
  return(
    <>
      <MovieProvider>
        <MovieHome/>
      </MovieProvider>
    </>
  )
}

export default Movie