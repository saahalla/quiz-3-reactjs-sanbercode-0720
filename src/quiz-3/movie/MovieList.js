import React, {useContext, useEffect} from "react"
import axios from "axios"
import {MovieContext} from "./MovieContext"

const MovieList = () =>{

  const [listMovie, setlistMovie] = useContext(MovieContext)

  useEffect( () => {
    if (listMovie.lists === null){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        setlistMovie({
          ...listMovie, 
          lists: res.data.map(el=>{ 
            return {id: el.id,
              title: el.title, 
              description: el.description, 
              year: el.year,
              duration: el.duration,
              genre: el.genre,
              rating: el.rating,
            }
          })
        })
      })
    }
  }, [setlistMovie])

  const handleEdit = (event) =>{
    let idDataMovie = parseInt(event.target.value)
    setlistMovie({...listMovie, selectedId: idDataMovie, statusForm: "changeToEdit"})
  }

  const handleDelete = (event) => {
    let idDataMovie = parseInt(event.target.value)

    let newLists = listMovie.lists.filter(el => el.id !== idDataMovie)

    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idDataMovie}`)
    .then(res => {
      console.log(res)
    })
          
    setlistMovie({...listMovie, lists: [...newLists]})
    
  }

  return(
    <>
      <h1 style={{textAlign: "center"}}>Table Film Film Terbaik</h1>
         <table style={{width: "70%", margin: '0 auto', border: "2px solid black", backgroundColor: "#f3f3f3"}}>
          <thead>
            <tr className="trlist">
              <th className="thlist">No</th>
              <th className="thlist">Title</th>
              <th className="thlist">Description</th>
              <th className="thlist">Year</th>
              <th className="thlist">Duration</th>
              <th className="thlist">Genre</th>
              <th className="thlist">Rating</th>
              <th className="thlist">Action</th>
            </tr>
          </thead>
          <tbody>
            {
              listMovie.lists !== null && listMovie.lists.map((item, index)=>{
                let jam = Math.floor(item.duration/60)
                if(jam == 0){
                  jam = ""
                }else{
                  jam = jam + " jam "
                }
                let menit = Math.floor(item.duration%60)
                if(menit == 0){
                  menit = ""
                }else{
                  menit = menit + " menit"
                }
                return(               
                  

                  <tr key={index} className="trlist">
                    <td className="tdlist">{index+1}</td>
                    <td className="tdlist">{item.title}</td>
                    <td className="tdlist">{item.description}</td>
                    <td className="tdlist">{item.year}</td>
                    <td className="tdlist">{jam + menit}</td>
                    <td className="tdlist">{item.genre}</td>
                    <td className="tdlist">{item.rating}</td>
                    
                    <td style={{display: "flex"}} className="tdlist">
                      <button onClick={handleEdit} value={item.id}>Edit</button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
            
          </tbody>
        </table>
    </>
  )
}

export default MovieList