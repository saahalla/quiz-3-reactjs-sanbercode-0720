import React, {useContext, useState, useEffect} from "react"
import axios from "axios"
import {LoginContext} from "./LoginContext"

const MovieForm = () =>{
  const [loginUser, setLoginUser] = useContext(LoginContext)
  const [user, setUser] = useState("")
  const [password, setPassword] = useState("")
  
  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()


  }

  const handleChangeUser = (event) =>{
    setUser(event.target.value)
    let tes = event.target.value
    // console.log(tes)
    console.log(user)
  }

  const handleChangePassword = (event) =>{
    setPassword(event.target.value)
    let tes = event.target.value
    console.log(tes)
  }

  return(
    <>
      <h1>Login Form</h1>
        <div style={{width: "40%", margin: "0 auto", marginTop: "50px", display: "block", backgroundColor: "#f3f3f3"}}>
            <form onSubmit={handleSubmit}>
            
            <div className="container">
                <label for="uname"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="uname" onChange = {handleChangeUser} required />

                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" onChange = {handleChangePassword} required />

                <button type="submit">Login</button>
                
            </div>

            </form>
        </div>
    </>
  )
}

export default MovieForm