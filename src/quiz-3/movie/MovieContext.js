import React, { useState, createContext } from "react";

export const MovieContext = createContext();

export const MovieProvider = props => {
  const [listMovie, setlistMovie] = useState({
    lists: null,
    selectedId: 0,
    statusForm: "create"
  });

  return (
    <MovieContext.Provider value={[listMovie, setlistMovie]}>
      {props.children}
    </MovieContext.Provider>
  );
};