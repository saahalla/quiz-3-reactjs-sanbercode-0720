import React, {useContext, useEffect} from "react"
import axios from "axios"
import {MovieContext} from "./MovieContext"

const MovieHome = () =>{

  const [listMovie, setlistMovie] = useContext(MovieContext)

  useEffect( () => {
    if (listMovie.lists === null){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        setlistMovie({
          ...listMovie, 
          lists: res.data.map(el=>{ 
            return {id: el.id,
              title: el.title, 
              description: el.description, 
              year: el.year,
              duration: el.duration,
              genre: el.genre,
              rating: el.rating,
            }
          })
        })
      })
    }
  }, [setlistMovie])
  
  return(
    <>
    <div style={{border: '2px solid #abc'}}>
      <h1 style={{textAlign: "center"}}>Daftar Film Film Terbaik</h1>
        {
          listMovie.lists !== null && listMovie.lists.map((item)=>{
            let jam = Math.floor(item.duration/60)
            if(jam == 0){
              jam = ""
            }else{
              jam = jam + " jam "
            }
            let menit = Math.floor(item.duration%60)
            if(menit == 0){
              menit = ""
            }else{
              menit = menit + " menit"
            }
            return(   
              <>
              <div style={{padding: "10px 10px 10px 30px" ,margin: "0 auto", width: "70%", border: '1px solid #abc', backgroundColor: "#f3f3f3"}}>
                <h2>{item.title + " (" + item.year + ")" }</h2>     
                <p>
                  <b>
                    {"Rating " + item.rating}<br/>
                    {"Durasi: " + jam + menit}<br/>
                    {"Genre: " + item.genre}<br/>
                  </b>              
                </p>   
                <p><b>Deskripsi :</b> {item.description}</p>
                        
              </div>
              </>
            )
          })
        }
      </div>
    </>
  )
}

export default MovieHome