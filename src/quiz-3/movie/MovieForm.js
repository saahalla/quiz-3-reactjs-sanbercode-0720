import React, {useContext, useState, useEffect} from "react"
import axios from "axios"
import {MovieContext} from "./MovieContext"

const MovieForm = () =>{
  const [listMovie, setlistMovie] = useContext(MovieContext)
  const [input, setInput] = useState({title: "", description: "", year: 2000, duration: 0, genre: "", rating: 1})

  useEffect(()=>{
    if (listMovie.statusForm === "changeToEdit"){
      let dataMovie = listMovie.lists.find(x=> x.id === listMovie.selectedId)
      setInput({title: dataMovie.title, description: dataMovie.description, year: dataMovie.year, duration: dataMovie.duration, genre: dataMovie.genre, rating: dataMovie.rating})
      setlistMovie({...listMovie, statusForm: "edit"})
    }
  },[listMovie])

  const handleChange = (event) =>{
    let typeOfInput = event.target.name

    switch (typeOfInput){
      case "title":
      {
        setInput({...input, title: event.target.value});
        break
      }
      case "description":
      {
        setInput({...input, description: event.target.value});
        break
      }
      case "year":
      {
        setInput({...input, year: event.target.value});
          break
      }
      case "duration":
      {
        setInput({...input, duration: event.target.value});
          break
      }
      case "genre":
      {
        setInput({...input, genre: event.target.value});
          break
      }
      case "rating":
      {
        setInput({...input, rating: event.target.value});
          break
      }
    default:
      {break;}
    }
  }
  
  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let title = input.title
    let description = input.description
    let year = input.year.toString()
    let duration = input.duration.toString()
    let genre = input.genre
    let rating = input.rating.toString()
    

    if (title.replace(/\s/g,'') !== "" && description.replace(/\s/g,'') !== "" &&
        year.replace(/\s/g,'') !== "" && duration.replace(/\s/g,'') !== "" && 
        genre.replace(/\s/g,'') !== "" && rating.replace(/\s/g,'') !== ""
    ){      
      if (listMovie.statusForm === "create"){        
        axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title, description, year, duration, genre, rating})
        .then(res => {
            setlistMovie(
              {statusForm: "create", selectedId: 0,
              lists: [
                ...listMovie.lists, 
                { id: res.data.id, 
                  title: input.title, 
                  description: input.description,
                  year: input.year,
                  duration: input.duration,
                  genre: input.genre,
                  rating: input.rating
                }]
              })
        })
      }else if(listMovie.statusForm === "edit"){
        axios.put(`http://backendexample.sanbercloud.com/api/movies/${listMovie.selectedId}`, {title, description, year, duration, genre, rating})
        .then(() => {
            let dataMovie = listMovie.lists.find(el=> el.id === listMovie.selectedId)
            dataMovie.title = input.title
            dataMovie.description = input.description
            dataMovie.year = input.year
            dataMovie.duration = input.duration
            dataMovie.genre = input.genre
            dataMovie.rating = input.rating
            setlistMovie({statusForm: "create", selectedId: 0, lists: [...listMovie.lists]})
        })
      }

      setInput({title: "", description: "", year: 0, duration: 0, genre: "", rating: 0})
    }

  }
  return(
    <>
      <h1>Form Create and Update Data Movie</h1>

      <div style={{width: "70%", margin: "0 auto", display: "block"}}>
        <div style={{padding: "20px"}}>
          <form onSubmit={handleSubmit}>
            <table style={{width: "100%", margin: '0 auto', backgroundColor: "#f3f3f3"}}>
              <tr className="trform">
                <td className="tdform">
                  <label for="title" style={{float: "left"}}>
                  Title:
                  </label> 
                </td>
                <td className="tdform">
                <input type="text" name="title" id="title" value={input.title} onChange={handleChange} required/>
                </td>
              </tr>
              <tr className="trform">
                <td className="tdform">
                  <label for="description" style={{float: "left"}}>
                  Description:
                  </label>
                </td>
                <td className="tdform">
                  <textarea type="text" name="description" id="description" value={input.description} rows="9" cols="80" onChange={handleChange} required>
              
                  </textarea>
                </td>
              </tr>
              <tr className="trform">
                <td className="tdform">
                  <label for="year" style={{float: "left"}}>
                  Year:
                  </label>
                </td>
                <td className="tdform">
                  <input type="number" name="year" id="year" value={input.year} min="1900" max="2020" onChange={handleChange}/>
                </td>
              </tr>
              <tr className="trform">
                <td className="tdform">
                  <label for="duration" style={{float: "left"}}>
                  Duration:
                  </label>
                </td>
                <td className="tdform">
                <input type="number" name="duration" id="duration" value={input.duration} onChange={handleChange}/>
                </td>
              </tr>
              <tr className="trform">
                <td className="tdform">
                <label for="genre" style={{float: "left"}}>
                Genre:
                </label> 
                </td>
                <td className="tdform">
                <input type="text" name="genre" id="genre" value={input.genre} onChange={handleChange} required/>
                </td>
              </tr>
              <tr className="trform">
                <td className="tdform">
                <label for="rating" style={{float: "left"}}l>
                Rating:
                </label>
                </td>
                <td className="tdform">
                <input type="number" name="rating" id="rating" min="1" max="10" value={input.rating} onChange={handleChange}/>
                </td>
              </tr>
              <tr className="trform">
                <td className="tdform">

                </td>
                <td className="tdform">
                <button style={{ float: "right"}}>submit</button>
                </td>
              </tr>
            </table>
                     
           
            
            
            {/* <input type="text" name='harga' value={getDescription} onChange={handleChangeDescription}/><br/> */}
                      
            
                      
            
                     
            
            
            
            {/* <div style={{width: "100%", paddingBottom: "20px"}}>
              
            </div> */}
          </form>
        </div>
      </div>
    </>
  )
}

export default MovieForm