import React, {useContext, useEffect} from "react"
import axios from 'axios'
import {MovieContext} from "./movie/MovieContext"

const MovieShow = () =>{
  const [listMovie, setlistMovie] = useContext(MovieContext)
    
  useEffect( () => {
    if (listMovie === null){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        console.log(res)
        setlistMovie(res.data.map(el=>{ 
          return {id: el.id, title: el.title, description: el.description, year: el.year, duration: el.duration, genre: el.genre, rating: el.rating}
        } ))
      })
    }
  }, [listMovie])

  return(
    <>
        <h1 style={{textAlign: "center"}}>Daftar Film Film Terbaik</h1>
         <table style={{width: "50%", margin: '0 auto', border: "2px solid black"}}>
           <thead>
             <tr>
                <th>No</th>
               <th>Title</th>
               <th>Description</th>
               <th>Year</th>
               <th>Duration</th>
               <th>Genre</th>
               <th>Rating</th>
               <th>Action</th>
             </tr>
           </thead>
            <tbody>
              {
                listMovie !== null && listMovie.map((item, index)=>{
                  return(                  
                    <tr key={index}>
                      <td>{index+1}</td>
                      <td>{item.title}</td>
                      <td>{item.description}</td>
                      <td>{item.duration}</td>
                      <td>{item.year}</td>
                      <td>{item.genre}</td>
                      <td>{item.rating}</td>
                      
                      
                    </tr>
                  )
                })
              }
              
            </tbody>
        </table>
    </>
  )

}

export default MovieShow