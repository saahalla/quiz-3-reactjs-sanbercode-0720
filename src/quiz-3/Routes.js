import React from "react";
import { Switch, Link, Route } from "react-router-dom";
import './routes.css'
import About from './about';
import Home from './movie/Home'
import Movie from './movie/Movie'
import Login from './movie/Login';

const Routes = () => {

  return (
    <>
    <body>
      <nav>
        <ul className="ulnav">
          <img src={require('./public/img/logo.png')} alt="logo" width="160px"/>
          <li className="linav">
            <Link to="/login">Login</Link>
          </li>
          <li className="linav">
            <Link to="/movie">Movie List Editor</Link>
          </li>
          <li className="linav">
            <Link to="/about">About</Link>
          </li>
          <li className="linav">
            <Link to="/">Home</Link>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route path="/movie">
          <Movie />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
      </body>
    </>
  );
};

export default Routes;
